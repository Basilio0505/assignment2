from flask import Flask, render_template, request, redirect, url_for, jsonify, json, Response
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
    r = json.dumps(books)
    return Response(r)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        create_key = request.form['newBook']
        books.append({'title': create_key, 'id':str(len(books)+1)})
        return redirect(url_for('showBook', nm = create_key))
    else:
        return render_template('newBook.html', books = books)

@app.route('/book/<int:book_id>/edit/', methods=['GET', 'POST'])
def editBook(book_id):
    if request.method == 'POST':
        edit_key = request.form['editName']
        for i in books:
            if i['id'] == str(book_id):
                i['title'] = edit_key
        return redirect(url_for('showBook', nm = edit_key))
    else:
        return render_template('editBook.html', books = books, book_id = book_id)
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        delete_key = request.form['Delete']
        for i in books:
            if i['id'] == str(book_id):
                books.remove(i)
        count = 1
        for i in books:
            i['id'] = str(count)
            count+=1
        return redirect(url_for('showBook', nm = delete_key))
    else:
        return render_template('deleteBook.html', books = books, book_id = book_id)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)